/** @method showFullImage
 *  @param event object
 */
showFullImage = (event) => {
    /** Remove existing image
     * In order to don't overload the application, remove current image or video
     */
    if (document.getElementById('current')) {
        document.getElementById('current').remove();
    }

    var i = event.target.id;
    var img = document.createElement('img');
    img.src = this.photos.hits[i].webformatURL;
    img.id = 'current';
    img.className = 'full-image';
    img.className = this.photos.hits[i].previewWidth > this.photos.hits[i].previewHeight ? 'full-image-h' : 'full-image-v';
    document.getElementById('gallery').style.display = 'none';

    document.getElementById('wrapper').appendChild(img);
    document.getElementById('app').className = 'blur';
}

/** @method hideImage
 */
hideImage = () => {
    if (document.getElementById('current')) {
        document.getElementById('current').remove();
        document.getElementById('app').classList.remove('blur');
        document.getElementById('gallery').style.display = 'block';
    }
}

/** @method showFullVideo
 *  @param event object
 */
showFullVideo = (event) => {
    /** Remove existing image
     * In order to don't overload the application, remove current image
     */
    if (document.getElementById('current')) {
        document.getElementById('current').remove();
    }

    var i = event.target.id;
    var vid = document.createElement('video');
    var source = document.createElement('source');
    source.src = this.videos.hits[i].videos.medium.url;
    vid.appendChild(source);
    vid.id = 'current';
    vid.className = 'full-video';
    document.getElementById('gallery').style.display = 'none';

    document.getElementById('wrapper').appendChild(vid);
    document.getElementById('app').className = 'blur';
    vid.play();
}

/** @method hideSpinner
 */
hideSpinner = () => {
    document.getElementById('app').classList.remove('blur');
    document.getElementById('spinner').style.display = 'none';
}

/** @method createImages
 */
createImages = () => {
    var quantity = this.getQuantity();
    if (this.photos.totalHits > 0) {
        for (let i in this.photos.hits) {
            if (i < quantity) {
                /** Creating gallery
                 * To avoid hardcode html images, this function create each image dynamically
                 */
                var img = document.createElement('img');
                img.src = this.photos.hits[i].previewURL;
                img.className = 'photo';
                img.id = i;
                img.onclick = showFullImage;
                document.getElementById('gallery').appendChild(img);
            }
        }
    }
}

/** @method createVideos
 */
createVideos = () => {
    var quantity = this.getQuantity();
    if (this.videos.totalHits > 0) {      
        for (let i in this.videos.hits) {
            if (i < quantity) {
                /** Creating gallery
                 * To avoid hardcode html images, this function create each image dynamically
                 * For videos, use a placeholder as preview image
                 */
                var img = document.createElement('img');
                img.src = '../assets/video-preview.jpg';
                img.className = 'photo';
                img.id = i;
                img.onclick = showFullVideo;
                document.getElementById('gallery').appendChild(img);
            }
        }
    }
}

/** @method getQuantity
 */
getQuantity = () => {
    var photosLength = this.photos.hits.length;
    var videosLength = this.videos.hits.length;
    var quantity = 0;
    
    /** Validate quantities
     * This function validate how much photos or videos must be showns
     */
    if (photosLength === 0 || videosLength === 0) {
        quantity = 15;
    } else {
        /** New Length
         * In case that photosLength - videosLength is higher than 0
         * Set quantity in value of the substraction
         * Else, if newLength is 0 and length is higher than 7
         * Set quantity in 7
         * Else, if newLength is 0
         * Set quantity in photosLength, because both had the same length
         */
        var newLength = photosLength - videosLength;
        if (Math.abs(newLength) > 0) {
            quantity = Math.abs(newLength);
        } else if (photosLength >= 8 && videosLength >= 8){
            quantity = 7;
        } else {
            quantity = photosLength;
        }
    }

    return quantity;
}

/** @method showAlert
 * @param text string
 */
showAlert = (text) => {
    document.getElementById('alert').classList.remove('hide');
    document.getElementById('alertText').textContent = text
    this.getPhotosVideos('cities');
    this.hideSpinner();

    /** Keep Alive
     * Keep alive the alert for 5 seconds
     */
    setTimeout(function () {
        this.hideAlert();
    }, 5000)
}

/** @method hideAlert
 */
hideAlert = () => {
    document.getElementById('alert').className = 'hide';
}