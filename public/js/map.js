/** @method createMap
 */
createMap = () => {
    /** Map creation
     * This function create a AmCharts map simple without information
     */
    this.map = AmCharts.makeChart( "chartdiv", {
        type: "map",
        theme: "chalk",
        projection: "miller",
        dragMap: false,
        dataProvider: {
          map: "worldLow",
          images: []
        },
        imagesSettings: {
            labelRollOverColor: "#000",
            labelPosition: "bottom"
        },
        areasSettings: {
          autoZoom: true,
          selectedColor: "#CC0000"
        },
        zoomControl: {
            homeButtonEnabled: false,
            zoomControlEnabled: false
        }
      } );
}