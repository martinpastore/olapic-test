/** @method httpRequest
 *  @param method string
 *  @param url string
 *  @param type string (undefined/null)
 */
httpRequest = (method, url, type) => {
    return new Promise (function (resolve, reject){
        var xhr = new XMLHttpRequest();

        if (type) {
            xhr.overrideMimeType(type);
        }

        xhr.open(method, url);

        xhr.onload = function() {
            resolve(xhr.responseText);
        };
    
        xhr.onerror = function() {
            reject(xhr.responseText);
        };

        xhr.send();
    });
}

/** @method getPosition
 */
getPosition = () => {
    httpRequest('GET', `/position`)
    .then( (data) => {
        const position = JSON.parse(data);
        /** Load ISS Position
         * Fill the images with a circle to locate ISS 
         */
        this.map.dataProvider.images.push({
            "selectable": true,
            "title": "ISS",
            "longitude": position.iss_position.longitude,
            "latitude": position.iss_position.latitude,
            "type": "circle",
            "color": "rgba(255,0,0,1)",
            "width": 16,
            "height": 16,
            "scale": 1,
            "fixedSize": false
        })
        /** Reload Map Information
         * Use this function to reload map information 
         * when the position was known
         */
        this.map.validateData();
        document.getElementById('position').textContent = 'ISS Position: ' + position.iss_position.latitude + ", " + position.iss_position.longitude;
        this.getCities(position);
    })
    .catch( (error) => {
        console.error(error);
    });
}

/** @method getCities
 *  @param data object
 */
getCities = (data) => {
    httpRequest('GET', `/cities?latitude=${data.iss_position.latitude}&longitude=${data.iss_position.longitude}`)
    .then( (resp) => {
        const cities = JSON.parse(resp);
        document.getElementById('nearby-cities').textContent = cities[0].length > 0 ? 'Total Nearby Cities: ' + cities.length : 'Total Nearby Cities: ' + 0;
        if (cities[0].length > 0) {
            let query = '';

            /** Build query string
             * In order to get photos for nearby cities,
             * you must to generate a query string to optimize the request.
             * Every city will be separated for +OR+
             */
            for (let i in cities) {
                const name = cities[i][1];
                const country = cities[i][3];
                
                if (i < cities.length - 1)
                    query += name + ' ' + country + '+OR+';
                else 
                    query += name + ' ' + country;
            }
            
            /** Query length
             * Reduce query if it is too long
             * The API only accept 100 chars.
             */
            if (query.length >= 100) {
                query = query.substring(0, 90);
            }
            this.getPhotosVideos(query);
        } else {
            this.showAlert("Sorry! We can't found cities near ISS, but here you have some amazing images and videos");
        }
    })
    .catch( (error) => {
        console.error(error);
    });
}

/** @method getPhotosVideos
 *  @param query string
 */
getPhotosVideos = (query) => {
    httpRequest('GET', `/photosvideos?cities=${query}`)
    .then( (resp) => {
        resp = JSON.parse(resp)
        this.photos = resp.photos;
        this.videos = resp.videos;
        
        if (this.photos.hits.length > 0 || this.videos.hits.length > 0) {
            this.createImages();
            this.createVideos();
            this.hideSpinner();
        } else {
            this.showAlert("Sorry! We can't found photos or videos of nearby cities, but here you have some amazing images and videos");
        }
    })
    .catch( (error) => {
        console.error(error);
    });
}