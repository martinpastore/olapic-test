const request = require('request');

/** @method getPosition
 *  @param req object
 *  @param res object
 */
exports.getPosition = (req, res) => {
    const opts = {
        url: `http://api.open-notify.org/iss-now.json`,
        headers: {},
        json: true
    }
    
    request(opts, async function(error, response, body) {
        if (error) throw new Error(error);
        
        res.send(body);
    });
}

/** @method getCities
 *  @param req object
 *  @param res object
 */
exports.getCities = (req, res) => {
    const opts = {
        /** Request to geobytes API
         * radius -> in km
         */
        url: `http://gd.geobytes.com/GetNearbyCities?minradius=0&radius=600&Latitude=${req.query.latitude}&Longitude=${req.query.longitude}`,
        headers: {
          'Access-Control-Allow-Origin': '*'
        },
        json: true
      }
    
      request(opts, async function(error, response, body) {
        if (error) throw new Error(error);

        res.send(body);
      });
}

/** @method getPhotos
 *  @param cities string
 */
exports.getPhotos = (cities) => {
    return new Promise(function (resolve, reject) {
        /** Request to pixabay
         * To optimize request, use per_page and image_type parameters
         */
        const opts = {
            url: `https://pixabay.com/api/?key=10223201-32156813d6c57ea0259fc4159&q=(${cities})&per_page=15&image_type=photo`,
            json: true
        }
        try {
            request(opts, async function(error, response, body) {
                resolve(body);
            });
        }
        catch (error) {
            reject(error);
        }
    });
}

/** @method getVideos
 *  @param cities string
 */
exports.getVideos = (cities) => {
    return new Promise(function (resolve, reject) { 
        const opts = {
            /** Request to pixabay
             * To optimize request, use per_page and video_type parameters
             */
            url: `https://pixabay.com/api/videos/?key=10223201-32156813d6c57ea0259fc4159&q=(${cities})&per_page=15&video_type=film`,
            json: true
        }
        try {
            request(opts, async function(error, response, body) {
                resolve(body);
            });
        }
        catch (error) {
            reject(error);
        } 
    });
}

/** @method getPhotosVideos
 *  @param req object
 *  @param res object
 */
exports.getPhotosVideos = async (req, res) => {
    /** Get Photos and Videos
     * Request to get photos and videos
     */
    var photos = await this.getPhotos(req.query.cities);
    var videos = await this.getVideos(req.query.cities);

    res.send({ photos: photos, videos: videos });
}