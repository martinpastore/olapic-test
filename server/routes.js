var express = require('express');
var router = express.Router();

var endpoints = require('./endpoints');

router.get('/position', endpoints.getPosition);
router.get('/cities', endpoints.getCities);
router.get('/photos', endpoints.getPhotos);
router.get('/videos', endpoints.getVideos);
router.get('/photosvideos', endpoints.getPhotosVideos);

module.exports = router;