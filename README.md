## ISS in Photos

### Application

To run the application, just use `npm start`

URL: http://olapic-martinpastore.herokuapp.com

#### Folder Structure
    | public
     - assets
     - js
     - styles
    | server

#### Frontend

**app.js**

Contains an autoexecutable function that call two methods

```javascript
createMap()
```
```javascript
getPosition()
```

**gallery.js**

It contains all methods related with image and video management

```javascript
/** 
 * event (object) taken from DOM
 */
showFullImage(event)
```

```javascript
hideImage()
```
```javascript
/** 
 * event (object) taken from DOM
 */
showFullVideo(event)
```
```javascript
hideSpinner()
```
```javascript
createImages()
```
```javascript
createVideos()
```
```javascript
getQuantity()
```
```javascript
/**
 * text (string) text to show in alert
*/
showAlert(text)
```
```javascript
getQuantity()
```

**map.js**

It contains all methods related with AmCharts library

```javascript
createMap()
```

**request.js**

It contains all API calls

```javascript
/**
* method (string) http method for API call.
* url (string) url to call
* type (can be undefined) (string) MimeType
*/
httpRequest(method, url, type)
``` 

```javascript
getPosition()
```
```javascript
/**
 * data (object) ISS information
 */
getCities(data)
```
```javascript
/**
 * query (string) cities and countries
 */
getPhotosVideos(query)
```

#### Middleware

It contains all external API calls and serves them as endpoints

**endpoints.js**

It contains all methods to request the information

```javascript
getPosition(req, res)
```
```javascript
getCities(req, res)
```
```javascript
/**
 * cities (string) string with all cities and countries
 */
getPhotos(cities)
```
```javascript
/**
* cities (string) string with all cities and countries
*/
getVideos(cities)
```
```javascript
getPhotosVideos(req, res)
```

**routes.js**

It contains all routes and endpoints calls


### Deployment

**gitlab-ci.yml**

 - git remote add heroku https://heroku:$HEROKU_API_KEY@git.heroku.com/olapic-martinpastore.git
    - `$HEROKU_API_KEY` -> Variable with API KEY to add heroku origin without password

 - git push -f heroku master
    - This command push to heroku repository and publicate in http://olapic-martinpastore.herokuapp.com
